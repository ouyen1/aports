# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=riot-web
pkgver=1.11.20
pkgrel=0
pkgdesc="A glossy Matrix collaboration client for the web"
url="https://riot.im/"
arch="noarch"
options="!check"
license="Apache-2.0"
source="https://github.com/vector-im/element-web/releases/download/v$pkgver/element-v$pkgver.tar.gz"
builddir="$srcdir/element-v$pkgver"

# secfixes:
#   1.11.7-r0:
#     - CVE-2022-39249
#     - CVE-2022-39250
#     - CVE-2022-39251
#     - CVE-2022-39236
#   1.11.4-r0:
#     - CVE-2022-36059
#     - CVE-2022-36060
#   1.9.7-r0:
#     - CVE-2021-44538
#   1.8.4-r0:
#     - CVE-2021-40823
#     - CVE-2021-40824

build() {
	return 0
}

package() {
	mkdir -p "$pkgdir"/usr/share/webapps \
		"$pkgdir"/etc/riot-web
	cp -r "$builddir" "$pkgdir"/usr/share/webapps/riot-web
	mv "$pkgdir"/usr/share/webapps/riot-web/config.sample.json \
		"$pkgdir"/etc/riot-web
	ln -sf /etc/riot-web/config.json \
		"$pkgdir"/usr/share/webapps/riot-web/config.json
}

sha512sums="
a4dd540fecaf44447a202191febdeaba8931f75ce45bdb83bbfbb00ae3b5f9c4c007b6beb86ca572e272bc9630832b3119d848afb8dc56430cfb67fd89db0567  element-v1.11.20.tar.gz
"
