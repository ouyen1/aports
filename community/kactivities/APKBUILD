# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivities
pkgver=5.102.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
pkgdesc="Core components for the KDE's Activities"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-or-later AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt5-qtbase-sqlite"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kwindowsystem-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	boost-dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7f379028bd839635692f6651687520aa7135173a267e1681707274fa1d1aba3b4f4d174da2275d20bef1d7831173d4b142c1d9a7dbc21c5c880402a32aca2ab9  kactivities-5.102.0.tar.xz
"
