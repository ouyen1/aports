# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=tig
pkgver=2.5.7
pkgrel=1
pkgdesc="Text-mode interface for the git revision control system"
url="https://jonas.github.io/tig/"
arch="all"
license="GPL-2.0-or-later"
depends="git"
makedepends="ncurses-dev pcre2-dev"
checkdepends="util-linux-misc"
subpackages="$pkgname-doc"
source="https://github.com/jonas/tig/releases/download/tig-$pkgver/tig-$pkgver.tar.gz
	implicit-decl.patch
	test-git-file-protocol.patch
	"

build() {
	export CFLAGS="$CFLAGS -D_BSD_SOURCE"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make all
}

check() {
	SHELL=/bin/sh script --return --quiet -c "make -j1 test" /dev/null
}

package() {
	make DESTDIR="$pkgdir" install install-doc-man
}

sha512sums="
26adbb8dc43ef7ec1eaf5d6def29a8b6dcf5e30825242fc489208139c8720805a265dd1ba412601df692f6d0438ee9e3171a3f8a3cc4599841d82dfc08a9ea1f  tig-2.5.7.tar.gz
bfab0bffe2dc1d8636fdc57eb1718498ddd6bd07cdc95be75b7f9f1fbfff5160c4fcc017e335d3e051d3f0893a79e21787330010fa75c2b3a18556f929c4b664  implicit-decl.patch
600d5eb15e9eeae558bf86caff028da83ff9c8bfc8956641b90fe42a00b483fadf4d419bdf687689b33739142fec7f3d0e160e6c7e29ff4887e61715b48db8e0  test-git-file-protocol.patch
"
