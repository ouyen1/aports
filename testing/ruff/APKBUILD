# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.0.235
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/charliermarsh/ruff"
# ppc64le, s390x, riscv64: ring
# x86, armhf, armv7: fails tests on 32-bit
arch="all !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
makedepends="maturin cargo py3-installer"
source="$pkgname-$pkgver.tar.gz::https://github.com/charliermarsh/ruff/archive/refs/tags/v$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	maturin build --release --frozen --manylinux off
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		target/wheels/*.whl
}

sha512sums="
579491065f7e4cd5b116c5f10ec604b8e160df01e9812efd584870807c78c7a4481182948ac1bcd0a3695427b8f5ba2a6fdb864f1b8436bfef61fcbcdb99c4a5  ruff-0.0.235.tar.gz
"
